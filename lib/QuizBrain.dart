import 'package:html_unescape/html_unescape.dart';
import 'package:quiz/Api.dart';
import 'package:quiz/Question.dart';
import 'package:quiz/QuizCategory.dart';
import 'package:quiz/Translator.dart';
import 'package:quiz/UserAnswer.dart';
import 'package:shared_preferences/shared_preferences.dart';

class QuizBrain {
  int _questionIndex = 0;
  int quizCategoryId = 15;
  var unescape = HtmlUnescape();
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  QuizBrain() {
    getQuizCategoryFromMemory();
  }

  void getQuizCategoryFromMemory() async {
    // SharedPreferences currentPrefs = await _prefs;
    _prefs.then((SharedPreferences prefs) {
      quizCategoryId = (prefs.getInt('quizCategoryId') ?? 0);
    });
  }

  Future<bool> getQuestions() async {
    questions = [];
    resetUserScore();
    Uri url = Uri.https('opentdb.com', '/api.php', {
      'amount': '10',
      'category': quizCategoryId.toString(),
      'type': 'boolean',
    });

    var response = await Api.getRequest(url);
    List<dynamic> quizArray = response['results'];

    String allQuestions = Translator.translatedArray(quizArray, 'question');

    String translateType = 'array';

    Map<String, dynamic> listOfPureTranslatedQuestion =
        await Translator.translate(allQuestions, translateType);

    for (int i = 0; i < quizArray.length; i++) {
      bool rightAnswer = quizArray[i]['correct_answer'] == 'True';
      questions.add(Question(
          questionText: unescape.convert(
              listOfPureTranslatedQuestion['translated_$translateType'][i]),
          questionAnswer: rightAnswer));
    }
    return false;
  }

  Future<void> getQuizThemes() async {
    Uri categoriesUrl = Uri.https('opentdb.com', '/api_category.php');
    dynamic response = await Api.getRequest(categoriesUrl);
    List<dynamic> categories = response['trivia_categories'];
    String pureStringOfCategories =
        Translator.translatedArray(categories, 'name');
    String translateType = 'array';

    Map<String, dynamic> listOfPureTranslatedCategories =
        await Translator.translate(pureStringOfCategories, translateType);

    for (int i = 0; i < categories.length; i++) {
      this.categories.add(QuizCategory(
          id: categories[i]['id'],
          name: unescape.convert(
              listOfPureTranslatedCategories['translated_$translateType'][i])));
    }
  }

  List<QuizCategory> categories = [];
  List<Question> questions = [];
  List<UserAnswer> answers = [];
  Question get currentQuestion {
    if (questions.length > 0) {
      return questions[_questionIndex];
    }
    return Question(
        questionAnswer: true,
        questionText:
            'Почему-то вопросы не загрузились, жалко да? А запросы норм прошли при этом');
  }

  bool get isNextQuestionExist {
    return !(_questionIndex + 1 >= questions.length);
  }

  void resetUserScore() {
    _questionIndex = 0;
    answers = [];
  }

  void changeQuizCategoryId(int id) async {
    SharedPreferences pref = await _prefs;
    pref.setInt('quizCategoryId', id);
    quizCategoryId = id;
    this.getQuestions();
  }

  String get howMuchRightAnswers {
    int rightAnswers = answers.where((element) => element.answer).length;
    double percentOfRightAnswers = rightAnswers / questions.length * 100;
    return percentOfRightAnswers.toStringAsFixed(2);
  }

  void setAnswer(bool userAnswer) {
    answers.add(UserAnswer(answer: userAnswer));
    if (isNextQuestionExist) {
      _questionIndex++;
    }
  }
}
