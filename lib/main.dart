import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:quiz/Api.dart';
import 'package:quiz/Drawer.dart';
import 'package:quiz/FinalQuestionAlert.dart';
import 'package:quiz/Question.dart';
import 'package:quiz/QuestionUserInterface.dart';
import 'package:quiz/QuizBrain.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  QuizBrain quizBrain = QuizBrain();
  _MyHomePageState() {
    Api.subscribers.add(setState);
    getQuestions();
    getThemes();
  }

  void getQuestions() async {
    await quizBrain.getQuestions();
  }

  void getThemes() async {
    await quizBrain.getQuizThemes();
  }

  Question get currentQuestion {
    return quizBrain.currentQuestion;
  }

  void submitUserAnswer(bool) {
    if (!quizBrain.isNextQuestionExist) {
      onCompleteModal();
    }
    setState(() {
      quizBrain.setAnswer(bool);
    });
  }

  String get percent {
    return quizBrain.howMuchRightAnswers;
  }

  bool get currentApiState {
    return Api.apiState;
  }

  Widget getFinalDialog(BuildContext context) {
    return FinalQuestionAlert(
        percent: quizBrain.howMuchRightAnswers,
        resetUserScore: () {
          setState(() {
            quizBrain.resetUserScore();
            Navigator.pop(context);
          });
        },
        getNewQuestions: () {
          setState(() {
            getQuestions();
            Navigator.pop(context);
          });
        });
  }

  Future<void> onCompleteModal() async {
    return showDialog(
        context: context, builder: getFinalDialog, barrierDismissible: false);
  }

  Widget currentAppState() {
    if (currentApiState) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    return SafeArea(
      child: Column(
        children: [
          Expanded(
            flex: 10,
            child: QuestionUserInterface(
              questionText: currentQuestion.questionText,
              questionRightAnswer: currentQuestion.questionAnswer,
              userAnswerCallBack: submitUserAnswer,
            ),
          ),
          Expanded(
            flex: 1,
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Row(
                children:
                    quizBrain.answers.map((e) => e.userAnswerIcon).toList(),
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // mainPageContext = context;
    return Scaffold(
      backgroundColor: Colors.white10,
      body: currentAppState(),
      drawer: HomePageDrawer(
        categories: quizBrain.categories,
        currentCategoryId: quizBrain.quizCategoryId,
        changeCategory: quizBrain.changeQuizCategoryId,
      ),
      drawerEdgeDragWidth: 100.0,
    );
  }
}
