import 'package:flutter/material.dart';
import 'package:quiz/QuizCategory.dart';

class HomePageDrawer extends StatefulWidget {
  final List<QuizCategory> categories;
  final int currentCategoryId;
  final void Function(int) changeCategory;
  HomePageDrawer(
      {this.categories, this.currentCategoryId, this.changeCategory});
  @override
  _HomePageDrawerState createState() {
    return _HomePageDrawerState(initialCategoryId: currentCategoryId);
  }
}

class _HomePageDrawerState extends State<HomePageDrawer> {
  int categoryId;
  _HomePageDrawerState({int initialCategoryId}) {
    this.categoryId = initialCategoryId;
  }
  Widget saveButton() {
    if (widget.currentCategoryId != categoryId) {
      return Container(
        margin: EdgeInsets.only(top: 10.0),
        child: TextButton(
          onPressed: () {
            Navigator.pop(context);
            widget.changeCategory(categoryId);
          },
          child: Text('Сохранить'),
          style: ButtonStyle(
            backgroundColor:
                MaterialStateColor.resolveWith((states) => Colors.green),
            foregroundColor:
                MaterialStateColor.resolveWith((states) => Colors.white),
          ),
        ),
      );
    }
    return Container();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Color.fromRGBO(97, 54, 142, 1),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            DropdownButton<QuizCategory>(
              style: const TextStyle(color: Colors.white),
              dropdownColor: Colors.purple,
              underline: Container(
                height: 2,
                color: Colors.white,
              ),
              onChanged: (QuizCategory category) {
                setState(() {
                  categoryId = category.id;
                });
              },
              value: widget.categories
                  .firstWhere((element) => element.id == categoryId),
              items: widget.categories
                  .map((e) => DropdownMenuItem(
                        child: Text(
                          e.name,
                          style:
                              TextStyle(fontSize: e.name.length > 20 ? 13 : 17),
                        ),
                        value: e,
                      ))
                  .toList(),
            ),
            saveButton(),
          ],
        ),
      ),
    );
  }
}
