import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class Api {
  static bool isLoading = false;
  static List<Function> subscribers = [];
  static List<Uri> listOfActiveUrlConnections = [];
  static Future<dynamic> getRequest(Uri url) async {
    if (listOfActiveUrlConnections.contains(url)) {
      return;
    }
    listOfActiveUrlConnections.add(url);
    var response = await http.get(url);
    if (response.statusCode == 200) {
      listOfActiveUrlConnections.remove(url);
      rerenderAllSubscribers();
      return convert.jsonDecode(response.body);
    }
    listOfActiveUrlConnections.remove(url);
    rerenderAllSubscribers();
    return '';
  }

  static bool get apiState {
    return listOfActiveUrlConnections.isNotEmpty;
  }

  static void rerenderAllSubscribers() {
    if (subscribers.isNotEmpty && listOfActiveUrlConnections.isEmpty) {
      for (int i = 0; i < subscribers.length; i++) {
        subscribers[i](() {});
      }
    }
  }
}
