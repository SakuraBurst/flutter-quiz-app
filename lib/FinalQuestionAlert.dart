import 'package:flutter/material.dart';

class FinalQuestionAlert extends StatelessWidget {
  final String percent;
  final void Function() getNewQuestions;
  final void Function() resetUserScore;
  FinalQuestionAlert({this.percent, this.getNewQuestions, this.resetUserScore});
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Вопросы закончилсь'),
      content: Text('Твой резульат $percent% правильных ответов'),
      actions: [
        TextButton(
          onPressed: () => getNewQuestions(),
          child: Text('Круто, хочу новые вопросы'),
          style: ButtonStyle(
            padding: MaterialStateProperty.resolveWith(
                (states) => EdgeInsets.all(10.0)),
            minimumSize:
                MaterialStateProperty.resolveWith((states) => Size(5000, 0)),
            enableFeedback: true,
            backgroundColor:
                MaterialStateColor.resolveWith((states) => Colors.cyan),
            foregroundColor:
                MaterialStateColor.resolveWith((states) => Colors.white),
          ),
        ),
        TextButton(
          onPressed: () => resetUserScore(),
          child: Text('Круто, хочу переиграть эти вопросы'),
          style: ButtonStyle(
            padding: MaterialStateProperty.resolveWith(
                (states) => EdgeInsets.all(10.0)),
            minimumSize:
                MaterialStateProperty.resolveWith((states) => Size(5000, 0)),
            enableFeedback: true,
            backgroundColor:
                MaterialStateColor.resolveWith((states) => Colors.cyan),
            foregroundColor:
                MaterialStateColor.resolveWith((states) => Colors.white),
          ),
        ),
      ],
    );
  }
}
