import 'package:quiz/Api.dart';

class Translator {
  static Future<dynamic> translate(String textToTranslate, String type) async {
    Uri url = Uri.https(
        'script.google.com',
        '/macros/s/AKfycbzq15WQaxYY8YeTJii_465cxdvQBEeWoJVB1RWALQPmGjyPoTTBVaWDkXQQYgfslLs/exec',
        {type: textToTranslate});

    return Api.getRequest(url);
  }

  static String translatedArray(List list, String key) {
    return list
        .map((e) => '"' + e[key] + '"')
        .toList()
        .toString()
        .replaceAll(new RegExp(r"^\[|\]$"), '');
  }
}
