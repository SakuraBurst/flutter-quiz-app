import 'package:flutter/material.dart';

class UserAnswer {
  bool answer;
  UserAnswer({this.answer});
  Widget get userAnswerIcon {
    IconData icon = answer ? Icons.check : Icons.close;
    Color color = answer ? Colors.green : Colors.red;
    return Icon(
      icon,
      color: color,
    );
  }
}
