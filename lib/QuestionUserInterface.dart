import 'package:flutter/material.dart';

class QuestionUserInterface extends StatelessWidget {
  final String questionText;
  final bool questionRightAnswer;
  final void Function(bool) userAnswerCallBack;
  QuestionUserInterface(
      {this.questionText, this.questionRightAnswer, this.userAnswerCallBack});
  submitUserAnswer(bool userAnswer) {
    userAnswerCallBack(userAnswer == questionRightAnswer);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Expanded(
          flex: 5,
          child: Padding(
            padding: EdgeInsets.all(20.0),
            child: Center(
              child: Text(
                questionText,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 20.0, vertical: 4.0),
            child: TextButton(
              onPressed: () => submitUserAnswer(true),
              child: Text('да'),
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateColor.resolveWith((states) => Colors.green),
                foregroundColor:
                    MaterialStateColor.resolveWith((states) => Colors.white),
              ),
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 20.0, vertical: 4.0),
            child: TextButton(
              onPressed: () => submitUserAnswer(false),
              child: Text('неа'),
              style: ButtonStyle(
                backgroundColor:
                    MaterialStateColor.resolveWith((states) => Colors.red),
                foregroundColor:
                    MaterialStateColor.resolveWith((states) => Colors.white),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
